DROP VIEW part_4;
CREATE VIEW part_4
AS 
SELECT
	domains.id as 'domain id',
	domains.name as 'domain name',
	records.name as 'record name',
	records.type as 'record type',
	records.content as 'record contant',
	users.username,
	cast(julianday('now') - julianday(datetime(records.change_date, 'unixepoch')) as integer) as 'created days ago'
FROM
	records
INNER JOIN domains ON domains.id = records.domain_id
INNER JOIN users ON users.id = domains.owner_id
WHERE records.type = 'A'
ORDER BY domains.id

#!/usr/bin/env python3

import requests
from concurrent.futures import ThreadPoolExecutor, as_completed

with open ("../artifacts/url-list-for-querying.txt", "r") as f:
    urls = f.read().splitlines()

def check(url):
    try:
        html = requests.get(url, stream=True)
    except:
        return {'url': url, 'status': -1}
    return {'url': url, 'status': html.status_code}

processes = []
with ThreadPoolExecutor(max_workers=10) as executor:
    for url in urls:
        processes.append(executor.submit(check, url))

for task in as_completed(processes):
    r = task.result()
    if r['status'] != 200:
        print(f"Fetch {r['url']} with status {r['status']}")

#!/usr/bin/env bash

if [[ $# -ge 1 ]]; then
	file_name="$1"
else
	echo "Usage: $0 file_to_parse"
	exit 1
fi

if [[ ! -f ${file_name} ]]; then
	echo "${file_name} doesn't exist"
	exit 2
fi

echo "awk: "
time awk -F'|' '{if ($3 ~ "ERROR") print }' "${file_name}"

echo -e "\n\ngrep: "
time grep -E '[^|]*\|[^|]*\|\s*ERROR' ${file_name}

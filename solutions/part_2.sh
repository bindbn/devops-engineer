#!/usr/bin/env bash

if [[ $# -ge 1 ]]; then
	file_name="$1"
else
	echo "Usage: $0 file_to_parse"
	exit 1
fi

if [[ ! -f ${file_name} ]]; then
	echo "${file_name} doesn't exist"
	exit 2
fi

echo "awk:"
time zcat "${file_name}" | awk '{print $1}' | sort | uniq -c | sort -n | tail -15

echo -e "\n\ncut: "
time zcat "${file_name}" | cut -f -1 -d ' ' | sort | uniq -c | sort -n | tail -15
